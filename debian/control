Source: aiohttp-wsgi
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: William Grzybowski <william@grzy.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-setuptools,
               python3-aiohttp,
               python3-sphinx
Standards-Version: 4.6.0
Homepage: https://github.com/etianen/aiohttp-wsgi
Vcs-Browser: https://salsa.debian.org/python-team/packages/aiohttp-wsgi
Vcs-Git: https://salsa.debian.org/python-team/packages/aiohttp-wsgi.git
Testsuite: autopkgtest-pkg-python

Package: python3-aiohttp-wsgi
Architecture: all
Depends: ${python3:Depends},
         python3-aiohttp,
         ${misc:Depends},
         ${sphinxdoc:Depends}
Description: WSGI adapter for aiohttp (Python 3)
 WSGI adapter for aiohttp.
 .
 Features:
  * Run WSGI applications (e.g. Django, Flask) on aiohttp.
  * Handle thousands of client connections, using asyncio.
  * Add websockets to your existing Python web app.

Package: aiohttp-wsgi-serve
Section: utils
Architecture: all
Depends: ${python3:Depends},
         python3-aiohttp-wsgi,
         ${misc:Depends}
Provides: httpd-wsgi3
Description: run a WSGI application
 Run a WSGI application on top of asyncio event loop.
 This script uses Python aiohttp-wsgi module to run existing
 WSGI applications within the event loop without further changes.
